import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import './main.css'

// import components
import TheMenu from '@/components/TheMenu.vue'
import TheHero from '@/components/TheHero.vue'
import MainButton from '@/components/templates/MainButton.vue'
import HeroTemplate from '@/components/templates/HeroTemplate.vue'
import FeaturesTemplate from '@/components/templates/FeaturesTemplate'
import NumbersFeatures from '@/components/templates/NumbersFeatures.vue'
import TheTestimonials from '@/components/TheTestimonials.vue'
import TheFeaures from '@/components/TheFeatures.vue'
import UsersTemplate from '@/components/templates/UsersTemplate.vue'
import TheCta from '@/components/TheCta.vue'
import CtaTemplate from '@/components/templates/CtaTemplate.vue'


const app = createApp(App)

  app.component('the-menu', TheMenu)
  app.component('the-hero', TheHero)
  app.component('main-button', MainButton)
  app.component('the-features', TheFeaures)
  app.component('hero-template', HeroTemplate)
  app.component('features-template', FeaturesTemplate)
  app.component('numbers-features', NumbersFeatures)
  app.component('the-testimonials', TheTestimonials)
  app.component('users-template', UsersTemplate)
  app.component('the-cta', TheCta)
  app.component('cta-template', CtaTemplate)

app.use(router)
app.mount('#app')
